"""
PROBLEM 5 / PSET 5
Munier Salem, Nov 10, 2014

"""

import pickle,os
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm

fig_dir,pick_dir= 'figures','pickles'
for dir in [fig_dir,pick_dir]:
  if not os.path.exists(dir): os.makedirs(dir)

# ================================================== 
# ================    PART A   ===================== 
# ================================================== 

# open data, graph returns, ACF/PACF ...
data = np.loadtxt('data/ibm2012.txt')

fig,axes = plt.subplots(3,2)
((ax1,ax2),(ax3,ax4),(ax5,ax6)) = axes 

# plot original data 
ax1.plot(data,'k-')
ax1.set_title('IBM 2012 Returns')
sm.graphics.tsa.plot_acf(data,lags=30,ax=ax3)
sm.graphics.tsa.plot_pacf(data,lags=30,ax=ax5)

# perform Box-Ljung test to confirm it's probably
# devoid of serial correlations ...
(lbvalue,pvalue) = sm.stats.diagnostic.acorr_ljungbox(data,lags=12)
print ">>> BOX-JLUNG up to lag 12 ... "
print "\t".join([ "%d" % (x+1) for x in range(len(lbvalue)) ])
print "\t".join([ "%.2f" % x for x in lbvalue ])
print "\t".join([ "%.2f" % x for x in pvalue ])

# ACF/PACF and Box-Lung suggest a lag 3 correlation,
# so let's fit an AR(3) model to this ...
pick_name = "pickles/IBM-AR3.pi"
try:
	AR3 = pickle.load(open(pick_name,'rb'))
except IOError:
	AR3 = sm.tsa.ARMA(data,(3,0)).fit(maxiter=100)
	pickle.dump(AR3,open(pick_name,'wb'))
Y = AR3.resid

# plot AR3 residuals ...
ax2.plot(Y,'k-')
sm.graphics.tsa.plot_acf(Y,lags=30,ax=ax4)
sm.graphics.tsa.plot_pacf(Y,lags=30,ax=ax6)

plt.savefig('figures/ibm.png')

# and again perform Box-Ljung ...
(lbvalue,pvalue) = sm.stats.diagnostic.acorr_ljungbox(Y,lags=12)
print ">>> AR3 BOX-LJUNG up to lag 12 ... "
print "\t".join([ "%.2f" % x for x in lbvalue ])
print "\t".join([ "%.2f" % x for x in pvalue ])


# ================================================== 
# ================    PART B   ===================== 
# ==================================================

# we test for serial correlations in square of 
# residuals for ARCH effect ...

# and again perform Box-Ljung ...
(lbvalue,pvalue) = sm.stats.diagnostic.acorr_ljungbox(Y**2,lags=12)
print ">>> RESID^2 BOX-LJUNG up to lag 12 ... "
print "\t".join([ "%.2f" % x for x in lbvalue ])
print "\t".join([ "%.1e" % x for x in pvalue ])


####### FOR PARTS C - E SEE R SCRIPT, PROBLEM-05.R ########
np.savetxt('data/IBM-resids.txt',Y)
