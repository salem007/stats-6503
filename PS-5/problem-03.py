"""
PROBLEM 3 / PSET 5
Munier Salem, Nov 10, 2014

	ADF Test on Lake Huron Data

"""

import numpy as np
import statsmodels.api as sm

data = np.loadtxt('data/lake.txt')

print "p\tADF\tp-val\tp-used"
print "-"*30
for p in [1,2]:
	adf,pvalue,usedlag,nobs,crit_vals,icbest = \
		sm.tsa.stattools.adfuller(data,maxlag=2)
	print "%d\t%.2f\t%.2f%%\t%d" % (p,adf,pvalue*100,usedlag)
