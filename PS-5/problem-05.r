# PROBLEM 5 part C - E
# Munier Salem 11.10.14

da = read.table('data/IBM-resids.txt')
library(fGarch)
m1 = garchFit(da~garch(1,0),data=da,trace=F)
summary(m1)
m2 = garchFit(da~garch(3,0),data=da,trace=F)
summary(m2)
m3 = garchFit(da~garch(1,0),data=da,trace=F,cond.dist='std')
summary(m3)
m4 = garchFit(da~garch(3,0),data=da,trace=F,cond.dist='std')
summary(m4)

predict(m3,5)
