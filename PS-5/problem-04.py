"""
PROBLEM 4 / PSET 5
Munier Salem, Nov 10, 2014

	Tries to find an optimal ARIMA model
	for airline passenger volume data,
	computes forecast and tests with 
	exogenous data ...

"""

import pickle,os
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm

fig_dir,pick_dir= 'figures','pickles'
for dir in [fig_dir,pick_dir]:
  if not os.path.exists(dir): os.makedirs(dir)

# ================================================== 
# ================       PART A       ==============
# ================================================== 

data = np.loadtxt('data/airshort.txt')

fig,((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)
#fig.set_size_inches(15,4)

# plot original data 
ax1.plot(data,'k-')
ax1.set_title('Airline passenger data 1949 - 1959')

# data's variation grows w/ level ... let's take ln
X = np.log(data)
ax2.plot(X,'k.-',markersize=5)
ax2.set_title('ln( raw data )')

# data appears to have trend and annual, seasonal
# component. Let's difference at lag 12 ... 
XX = X[12:]-X[:-12]
ax3.plot(XX,'k-')
ax3.set_title('ln data differenced at lag 12')

# From ACF of this, it appears this has a root
# close to z = 1 ... let's difference at lag 1
Y = XX[1:] - XX[:-1]
ax4.plot(Y,'k-')
ax4.set_title('ln data differenced at 12 and 1')
plt.savefig('figures/airline-series.eps')

# Look at ACF / PACF for our transformed data
plt.clf()
fig,axes = plt.subplots(2,2)
((ax1,ax2),(ax3,ax4)) = axes
sm.graphics.tsa.plot_acf(XX,lags=30,ax=ax1)
sm.graphics.tsa.plot_pacf(XX,lags=30,ax=ax2)
sm.graphics.tsa.plot_acf(Y,lags=30,ax=ax3)
sm.graphics.tsa.plot_pacf(Y,lags=30,ax=ax4)
titles=['ACF D12','PACF D12','ACF D1*D12','PACF D1*D12']
for ax,title in zip(axes.ravel(),titles): ax.set_title(title)
plt.savefig('figures/airline-acf-pacf.png')

# suggests we should check out AR
# models with lags up to 12 ...
p,q = 15,0
pick_name = "pickles/arma%d%d.pi" % (p,q)
try:
	arma_mod = pickle.load(open(pick_name,'rb'))
except IOError:
	arma_mod = sm.tsa.ARMA(Y,(p,q)).fit(maxiter=100)
	pickle.dump(arma_mod,open(pick_name,'wb'))
resids = arma_mod.params/(1.96*np.sqrt(arma_mod.sigma2))
print arma_mod.params
print resids

plt.clf()
plt.plot(np.ones(resids.shape),'k--')	
plt.plot(-np.ones(resids.shape),'k--')	
plt.plot(resids,'k.-',markersize=10)
plt.savefig('figures/arma15.eps')

# check out AIC / BIC for a host of AR 
# model choices ...
pick_name = "pickles/AR-models.pi"
try:
	models = pickle.load(open(pick_name,'rb'))
except IOError:
	models = []
	q = 0
	for p in range(1,15):
		models.append((p,sm.tsa.ARMA(Y,(p,q)).fit()))
	pickle.dump(models,open(pick_name,'wb'))

print "\n\n\tAR MODEL SUMMARY:"
print "\tp\tAIC\tBIC"
print "\t" + "-"*20
for p,model in models:
	print "\t%d\t%d\t%d" % (p,model.aic,model.bic)


# DO THE SAME, for MA processes ...
pick_name = "pickles/MA-models.pi"
try:
	models2 = pickle.load(open(pick_name,'rb'))
except IOError:
	models2 = []
	p = 0
	for q in range(1,13):
		try:
			models2.append((q,sm.tsa.ARMA(Y,(p,q)).fit()))
		except ValueError:
			print "WARNING: q = %d was non invertible" % q
	pickle.dump(models2,open(pick_name,'wb'))

print "\n\n\tMA MODEL SUMMARY:"
print "\tp\tAIC\tBIC"
print "\t" + "-"*20
for q,model in models2:
  print "\t%d\t%d\t%d" % (q,model.aic,model.bic)


# ====================================================
# ==============       PART B           ============== 
# ====================================================


# ====================================================
# ==============       PART C/D/E       ============== 
# ====================================================

# plot / print results for best MA and best AR 
# model ...

for model,mod_label in zip([models[10],models2[11]],['AR','MA']):
	order,model = model
	# load in the complete AIRPASS data ...
	d2 = np.log(np.loadtxt('data/airpass.txt'))
	Y2 = d2[12:]-d2[:-12]
	Y2 = Y2[1:]-Y2[:-1] 

	fig,((ax1),(ax2)) = plt.subplots(2,1)
	fig.set_size_inches(15,8)
	model.plot_predict(0,len(Y)+12,ax=ax1)
	ax1.plot(Y2,'g-')

	# print predictions / bounds / actual
	forecast, fcasterr, conf_int = model.forecast(steps=12,alpha=.05)
	prediction = model.predict(0,len(Y)+12)
	fx = len(forecast) + np.cumsum(np.ones(len(forecast)))
	ax2.plot(fx,forecast,'b-',linewidth=3)
	ax2.plot(fx,conf_int[:,0],'b--')
	ax2.plot(fx,conf_int[:,1],'b--')
	ax2.plot(Y2[-25:],'g-')
	plt.savefig('figures/airline-predict-%s.png' % mod_label )
	
	print "%s-%d Forecast for 1960:" % (mod_label,order)
	print "-"*50
	print "Y^\tY+\tY-\tY\t?"
	print "="*50
	for F,(CLO,CHI),Y2 in zip(forecast,conf_int,Y2[-13:]):
		success = Y2 > CLO and Y2 < CHI
		error = F - Y2
		print ("%.03f\t"*5+"%s") % (F,CLO,CHI,Y2,error,success) 
#print forecast
#print prediction[-13:]
