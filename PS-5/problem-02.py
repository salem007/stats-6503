"""
PROBLEM 2 / PSET 5
Munier Salem, Nov 10, 2014

	Part (i) of B&D #9.6 -- Lake Huron Data

"""

import pickle,os
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm

fig_dir,pick_dir= 'figures','pickles'
for dir in [fig_dir,pick_dir]:
  if not os.path.exists(dir): os.makedirs(dir)

data = np.loadtxt('data/lake.txt')

fig,axes = plt.subplots(3,2)
((ax1,ax2),(ax3,ax4),(ax5,ax6)) = axes 

# plot original data 
ax1.plot(data,'k-')
ax1.set_title('Lake Huron Levels')

# from ACF slow decay seems to have trend ...
# let's difference ...
Y = data[1:]-data[:-1]
ax2.plot(Y,'k-')
ax2.set_title('First Difference')
print "SIGMA Y: ",np.std(Y)

# Look at ACF / PACF for our transformed data
sm.graphics.tsa.plot_acf(data,lags=30,ax=ax3)
sm.graphics.tsa.plot_pacf(data,lags=30,ax=ax5)
sm.graphics.tsa.plot_acf(Y,lags=30,ax=ax4)
sm.graphics.tsa.plot_pacf(Y,lags=30,ax=ax6)
#titles=['','ACF raw','PACF raw','ACF D1','PACF D1']
#
#for ax,title in zip(axes.ravel(),titles): ax.set_title(title)
plt.savefig('figures/lake.png')

# ACF / PACF suggests an MA model is apropriate,
# perhaps with lag 2  ... let's try a 10
# and see ratios ...
for v,p,q in [('MA',0,10),('AR',10,0)]:
	p,q = 0,10
	pick_name = "pickles/lake-%s%d.pi" % (v,max(q,p))
	try:
		arma_mod = pickle.load(open(pick_name,'rb'))
	except IOError:
		arma_mod = sm.tsa.ARMA(Y,(p,q)).fit(maxiter=100)
		pickle.dump(arma_mod,open(pick_name,'wb'))
	resids = arma_mod.params/(1.96*np.sqrt(arma_mod.sigma2))
	print "="*20 + " %s" % v
	print	"\t".join([ "%.02f"%x for x in arma_mod.params ])
	print	"\t".join([ "%.02f"%x for x in resids ])


plt.clf()
plt.plot(np.ones(resids.shape),'k--')	
plt.plot(-np.ones(resids.shape),'k--')	
plt.plot(resids,'k.-',markersize=10)
plt.savefig('figures/lake-MA20.eps')
