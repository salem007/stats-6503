"""
	PROBLEM 2 / 3, PROBLEM SET 8
	by Munier Salem, Nov. 22, 2014

	Analyzes volume and price movements
	of high frequency Boeing data from
	Dec. 1, 2012.

"""

from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt 
import statsmodels.api as sm

year,month,day = 2012,12,1
ohour,omin = 9,30

# load boeing trade data
raw = np.loadtxt('data/taq-td-ba12012008.txt',skiprows=1)
volume = raw[:,4]
times = (raw[:,0]*60 + raw[:,1]) + raw[:,2]/60.0 
times = times - ohour*60 - omin

# ================================================== 
# ===================   PROBLEM 2  ================= 
# ================================================== 

# bin into 5-minute increments ...
bins = np.arange(9.5*60,16*60.0,5.0) # 9:30 am - 4 pm in 5 min 
periods = np.array([datetime(year,month,day,int(np.floor(t/60.0)),int(t%60.0)) for t in bins ])
binned_volume = np.zeros(bins.shape)
for t,v in zip(times,volume):
	binned_volume[int(np.floor(t/5.0))] += v	

# plot results
fig,((ax1,ax2)) = plt.subplots(2,1) 
ax1.plot(periods,binned_volume,'k-',linewidth=2)
ax1.set_xlim(datetime(year,month,day,9,00),datetime(year,month,day,16,00))
ax1.set_xlabel('time')
ax1.set_ylabel('volume')
ax1.set_title('Dec. 1 2012 Boeing Stock Trading Volume')
"""
# plot ACF
sm.graphics.tsa.plot_acf(volume,lags=60,ax=ax2)
ax2.set_ylim(0,0.06)
ax2.set_xlabel('lag (in 5 minute inc)')
ax2.set_ylabel('ACF of Volume')
ax2.set_title('')

# compute ACF values ...
vol_ACF = sm.tsa.stattools.acf(volume)
print "min\tACF"
print "-"*20
for i,a in enumerate(vol_ACF):
	if a > 1e-2:
		print "%02d\t%.2e" % (i*5,a)

plt.savefig('figures/trading-volume')
"""
# ================================================== 
# ===================   PROBLEM 3  ================= 
# ==================================================

plt.clf()
fig,((ax1,ax4),(ax2,ax5),(ax3,ax6)) = plt.subplots(3,2)
fig.set_size_inches(15,15)

# compute final price in ea. 5 min interval
raw_price = raw[:,3]
price = np.zeros(bins.shape)
for t,p in zip(times,raw_price):
  price[int(np.floor(t/5.0))] = p
ax1.plot(periods,price)
ax1.set_xlabel('time')
ax1.set_ylabel('price')
ax1.set_title('5 minute periods')

# compute log returns 
returns = np.log10(price)
returns = returns[1:]-returns[:-1]
ax2.plot(periods[1:],returns)
ax2.set_xlabel('time')
ax2.set_ylabel('returns')

# plot ACF ...
sm.graphics.tsa.plot_acf(returns,lags=60,ax=ax3)
ax3.set_title('')
ax3.set_ylabel('ACF')
ax3.set_xlabel('lag (5 min inc.)')


## REPEAT ... with 10 min intervals ...

bins = np.arange(9.5*60,16*60.0,10.0) # 9:30 am - 4 pm in 5 min 
periods = np.array([datetime(year,month,day,int(np.floor(t/60.0)),int(t%60.0)) for t in bins ])
price = np.zeros(bins.shape)
for t,p in zip(times,raw_price):
  price[int(np.floor(t/10.0))] = p
ax4.plot(periods,price)
ax4.set_xlabel('time')
ax4.set_ylabel('price')
ax4.set_title('10 minute periods')

# compute log returns 
returns = np.log10(price)
returns = returns[1:]-returns[:-1]
ax5.plot(periods[1:],returns)
ax5.set_xlabel('time')
ax5.set_ylabel('returns')

# plot ACF ...
sm.graphics.tsa.plot_acf(returns,lags=30,ax=ax6)
ax6.set_title('')
ax6.set_ylabel('ACF')
ax6.set_xlabel('lag (10 min inc.)')

plt.savefig('figures/boeing-returns')


# finally, let's compute the percentage of transactions
# where the price hadn't moved from previous period ...
same_price_vol = np.sum(volume[raw_price[1:] == raw_price[:-1]])
total_vol = np.sum(volume)
print "SAME PRICE:", same_price_vol
print "TOTAL VOL: ", total_vol
print "PERCENTAGE:", same_price_vol/total_vol * 100
