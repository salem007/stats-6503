"""
	PROBLEM 4 / PS 8
	Munier Salem, Nov. 23, 2014
	
	Tsay 6.6
"""

import numpy as np
import matplotlib.pyplot as plt

mu = .2
s  = .4
P0 = 60
t  = 2

# sample from distribution of log returns
returns = np.random.normal((mu-.5*s**2)*t,s*np.sqrt(t),size=10000)

# transform into final prices ...
prices = P0 * np.exp(returns)

# compute moments ...
mean = np.average(prices)
std = np.std(prices)
lower = np.percentile(prices,2.5)
upper = np.percentile(prices,97.5)

print "     MEAN: $%.2f" % mean
print "      STD: $%.2f" % std
print "95%% CONF: $%.2f - $%.2f" % (lower,upper)

# show histogram ...
plt.hist(prices,bins=50)
plt.title('Distribution of 10,000 sampled prices, two years out')
plt.xlabel('price')
plt.savefig('figures/histogram.eps')
