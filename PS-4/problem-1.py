"""
	PROBLEM 1: 
		Brockwell & Davis Problem 5.8
	Oct. 20, 2014
	Munier Salem

	COMPUTES FORECAST FOR X-PROCESS using
	innovation algorithm.
"""

import os
import numpy as np
import matplotlib.pyplot as plt

figMainDir,figDir = 'figures','figures/problem-1'
for dir in [figMainDir,figDir]:
  if not os.path.exists(dir): os.makedirs(dir)


def gamma(h):
	""" Autocovariance fcn """
	return .5**abs(h) * ( 2.963 - .7822*abs(h) )


def theta(n):
	""" MA(1) coefficients """
	if n == 0: return 1.0
	if n == 1: return -0.7
	return 0.0


def phi(n):
	""" AR(2) coefficients """
	if n == 0: return 1.0
	if n == 1: return 0.1
	if n == 2: return -0.7
	return 0.0 


def kappa(i,j):
	""" matrix used in innovation algorithm """
	if i <= 2 and j <= 2:
		return gamma(i-j)
	if min(i,j) <= 2 and max(i,j) >= 2 and max(i,j) <= 4:
		return gamma(i-j) - 0.1*gamma(1-abs(i-j)) - .12*gamma(2-abs(i-j))
	if min(i,j) > 2:
		return theta(abs(i-j)) - 0.7*theta(1+abs(i-j))
	return 0.0


def innovation(n_max=10):
	""" computes THETA and V for forecasting """
	v = np.zeros(n_max+1)
	THETA = np.zeros((n_max+1,n_max+1))
	v[0] = kappa(1,1)
	
	for n in range(1,n_max+1):
		# compute Theta's ...
		for k in range(n):
			tmp = kappa(n+1,k+1)
			for j in range(k):
				tmp -= THETA[k,k-j]*THETA[n,n-j]*v[j]
			THETA[n,n-k] = tmp/v[k]
		# compute v ...	
		tmp = kappa(n+1,n+1)
		for j in range(n):
			tmp -= (THETA[n,n-j])**2*v[j]
		v[n] = tmp

	return v,THETA


# ================================================== 
# ============= SCRIPT STARTS HERE ================= 
# ================================================== 

# plot autocovariance function ...
print "\nKAPPA MATRIX"; print "-"*20
h   = np.arange(0,10)
acf = gamma(h)
plt.plot(h,acf)
plt.savefig(figDir + '/acf.eps')

# print kappa(i,j):
for i in range(1,10):
	print "%02d\t" % i,
	for j in range(1,10):
		print "%0.3f\t" % kappa(i,j),
	print ""


# COMPUTE V's AND THETA'S
n_max = 11
v,THETA = innovation(n_max=n_max)

# compute forecast given our data
x = [0.0,.644,-.442,-.919,-1.573,.852,-.907,.686,-.753,-.954,.576,0.0]

X_HAT = np.zeros(n_max+1)
X_HAT[1] = 0.0
X_HAT[2] = THETA[1,1]*x[1]
for n in range(2,n_max):
	X_HAT[n+1] = phi(1)*x[n] + phi(2)*x[n-1] + THETA[n,1]*(x[n]-X_HAT[n])

# Compute forecast beyond n+1 ...
#              P10*X10,  P10*X11,    P10*X12, P10*X13 
P = np.array([ X_HAT[n], X_HAT[n+1], 0.0,     0.0 ])
for i in range(2,4):
	P[i] = phi(1)*P[i-1] + phi(2)*P[i-2] 

# display results:
print "\nN\tXn\tTn1\tTn2\tTn3\tv_n\tX_hat,n"
print "-"*60
for n in range(n_max+1):
	print "%2d\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f" \
		% (n,x[n],THETA[n,1],THETA[n,2],THETA[n,3],v[n],X_HAT[n])
for i,p in enumerate(P[2:]):
	print "%2d\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f" \
		% (n+i+1,0.0,0.0,0.0,0.0,0.0,p)

# plot X and X-HAT
X_HAT = np.concatenate([X_HAT,P[2:]])
plt.clf()
plt.plot(range(1,len(x)-1),x[1:-1])
plt.plot(range(1,len(X_HAT)),X_HAT[1:])
plt.legend(['x','x-hat'],loc='best')
plt.xlabel('t')
plt.savefig(figDir+'/x-xhat.eps')
