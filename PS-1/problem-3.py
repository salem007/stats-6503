"""
STATS 6503
HW 1, PROBLEM 3 

Munier Salem
09.21.2014

"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats

# load data and take log returns of the S&P
data = np.loadtxt('data/ge.txt',skiprows=1)
returns = np.log(1.0+data[:,-1]) 

# Plot histogram of log returns ...
plt.hist(returns,100,facecolor='gray',edgecolor='grey')
plt.xlabel('S&P daily log returns')
plt.title('Distribution of S&P\'s Daily Log Returns')
plt.savefig('figures/snp.eps')

# Perform hypothesis tests ...
print "TEST\tSCORE\t\tP-Val"; print "-"*40
print "MEAN\t%.2e\t%.2e\t" % ( stats.ttest_1samp(returns,0.0) )
print "SKEW\t%.2e\t%.2e\t" % ( stats.skewtest(returns) )
print "KURT\t%.2e\t%.2e\t" % ( stats.kurtosistest(returns) )
