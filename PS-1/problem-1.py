import numpy as np
import scipy.stats as stats

# load data ...
fName,IDX = 'data/amex.txt','AMEX'
data = np.loadtxt(fName,skiprows=1)
indexes = [IDX,'VW','EW','S&P']


# compute mean, variance, skew and kurtosis
# for both the simple returns and log returns
# of each time series ...

def desc_stats(idx,returns):
	print "%4s\t%.2e\t%.2e\t%.2e\t%.2e\t%.2e\t%.2e" % \
		( idx, np.mean(returns), np.sqrt(np.var(returns)),
			stats.skew(returns),
			stats.kurtosis(returns,fisher=True),
			np.min(returns),np.max(returns))

print "IDX\tMEAN\t\tS-DEV\t\tSKEW\t\tKURT\t\tMIN\t\tMAX"

print "SIMPLE  " + "-"*90
for i,idx in enumerate(indexes):
	desc_stats(idx,data[:,i+1])
print "LOGARMC " + "-"*90
for i,idx in enumerate(indexes):
	desc_stats(idx,np.log(1.0+data[:,i+1]))


# next, for the log returns, test if the mean
# of the IDX's returns is in fact zero
t,p = stats.ttest_1samp(np.log(1.0+data[:,1]),0.0)
print "\n\nMEAN HYP TEST:"
print "\tt,p = %.3g , %.3g" % (t,p)
