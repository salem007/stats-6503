"""
PROBLEM 2 / PSET 6
Munier Salem, Nov 16, 2014

"""

import pickle,os
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm

fig_dir,pick_dir= 'figures','pickles'
for dir in [fig_dir,pick_dir]:
  if not os.path.exists(dir): os.makedirs(dir)

data = np.loadtxt('data/djao2.txt')[:,1]

# find returns ...
Y = np.log(data)
Y = Y[1:]-Y[:-1]

fig,axes = plt.subplots(3,3)
((ax1,ax2,ax7),(ax3,ax4,ax8),(ax5,ax6,ax9)) = axes 
fig.set_size_inches(15,15)

# plot original data 
ax1.plot(data,'k-')
ax1.set_title('DJA02')

ax2.plot(Y,'k-')
ax2.set_title('Log Returns')
print "SIGMA Y: ",np.std(Y)
# Look at ACF / PACF for our transformed data
sm.graphics.tsa.plot_acf(data,lags=30,ax=ax3)
sm.graphics.tsa.plot_pacf(data,lags=30,ax=ax5)
sm.graphics.tsa.plot_acf(Y,lags=30,ax=ax4)
sm.graphics.tsa.plot_pacf(Y,lags=30,ax=ax6)
#titles=['','ACF raw','PACF raw','ACF D1','PACF D1']
#
#for ax,title in zip(axes.ravel(),titles): ax.set_title(title)

# ACF / PACF suggests an MA model is apropriate,
# with lag 7  ... 
p,q = 0,7
pick_name = "pickles/dja02-MA%d.pi" % q
try:
	arma_mod = pickle.load(open(pick_name,'rb'))
except IOError:
	arma_mod = sm.tsa.ARMA(Y,(p,q)).fit(maxiter=100)
	pickle.dump(arma_mod,open(pick_name,'wb'))
ratios = arma_mod.params/(1.96*np.sqrt(arma_mod.sigma2))
print	"\t".join([ "%.02f"%x for x in arma_mod.params ])
print	"\t".join([ "%.02f"%x for x in ratios ])


# look at residuals ...
resids = arma_mod.resid
ax7.plot(resids,'k-')
ax7.set_title('MA7 Resids')
sm.graphics.tsa.plot_acf(resids,lags=30,ax=ax8)
sm.graphics.tsa.plot_pacf(resids,lags=30,ax=ax9)
plt.savefig('figures/dja02.png')


# check out square of the data ...
plt.clf()
square = resids**2
fig,((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)
ax1.plot(square,'k-')
sm.graphics.tsa.plot_acf(square,lags=30,ax=ax2)
sm.graphics.tsa.plot_pacf(square,lags=30,ax=ax3)
plt.show()
