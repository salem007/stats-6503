"""
TSAY PROBLEM 8.1 parts A and C

Munier Salem, November 2, 2014
"""

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('data/m-ibmvwewsp2603.txt',skiprows=1)

# plot returns over time ...
symbols = ['IBM','VW','EW','SP']
fig,axes = plt.subplots(4,1)
for x,ax,title in zip(data.T[1:],axes.ravel(),symbols):
	ax.plot(x,'k-')
	ax.set_ylabel(title)
plt.savefig('figures/problem-2-stocks.eps')

# compute mean / CC matrix
print "MEANS:",np.average(data.T[1:],axis=1)
print "COV:\n",np.cov(data.T[1:])
print "CORR:\n",np.corrcoef(data.T[1:])

r_crit = 2.0/np.sqrt(len(data[1:]))
# display which corr's are significant ...
for lag in range(1,5):
	print "LAG %d" % lag
	for row in range(1,5):
		for col in range(1,5):
			rho = np.corrcoef(data[lag:,row],data[:-lag,col])[1,0],
			if rho > r_crit:
				print "+\t",
			elif rho < -r_crit:
				print "-\t",
			else:
				print ".\t",
		print 
#corr = np.corrcoef(data.T[1:])
#crits = np.chararray(corr.shape)
#crits[:] = '.'
#print crits
