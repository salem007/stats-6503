"""
TSAY PROBLEM 9.5

Munier Salem, November 2, 2014
"""

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('data/m-fac9003.txt',skiprows=1)

# plot returns over time ...
symbols = ['AA','AGE','CAT','F','FDX','GM','HPQ','KMB','MEL','NYT','PG','TRB','TXN','SP5']
fig,axes = plt.subplots(14,1)
fig.set_size_inches(5,10)
for x,ax,title in zip(data.T,axes.ravel(),symbols):
	ax.plot(x,'k-')
	ax.set_ylabel(title)
	ax.set_xticks([])
	ax.set_yticks([])
plt.savefig('figures/problem-5-stocks.eps')

# do PCA with correlation matrix ...
def vecPrint(V):
	print "\t".join([ "%.2f" % x for x in V ])
def matPrint(M): 
	for row in M: vecPrint(row)

def PCA(M):
	l,v = np.linalg.eig(M)
	proportion = l/np.sum(l)
	print "EIGENVALUES:"; vecPrint(l)
	print "PROPORTIONS:"; vecPrint(proportion)
	print "CUMULATIVE: "; vecPrint(np.cumsum(proportion))
	print "EIGENVECTOR:"; matPrint(v)
	return l,v

corr = np.corrcoef(data.T)
print "="*50
print "CORRELATION MATRIX:"; matPrint(corr)
l,v = PCA(corr)

# produce scree plot
ls = l.copy(); ls.sort(); ls = ls[::-1]
plt.clf()
fig,ax = plt.subplots(1,1)
fig.set_size_inches(16,10)
plt.plot(np.cumsum(np.ones(ls.shape)),ls,'ks-',markersize=10,linewidth=2)
plt.xlabel('factor'); plt.ylabel('eigenvalue')
plt.xlim(0,15)
plt.savefig('figures/scree.eps') 
