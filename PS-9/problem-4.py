"""
TSAY PROBLEM 9.2

Munier Salem, November 2, 2014
"""

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('data/m-5clog-9008.txt',skiprows=1)

# plot returns over time ...
symbols = ['IBM', 'HPQ', 'INTC', 'JPM', 'BAC']
fig,axes = plt.subplots(5,1)
for x,ax,title in zip(data.T,axes.ravel(),symbols):
	ax.plot(x,'k-')
	ax.set_ylabel(title)
plt.savefig('figures/problem-4-stocks.eps')

# plot relation between returns ...
plt.clf()
fig,axes = plt.subplots(5,5)
fig.set_size_inches(8,8)
for row in range(5):
	axes[row, 0].set_ylabel(symbols[row])
	axes[-1,row].set_xlabel(symbols[row])
	for col in range(5):
		ax = axes[row,col]
		if row == col:
			ax.hist(data[:,row],edgecolor='white')
		else:
			ax.plot(data[:,row],data[:,col],'b.',alpha=.2)
		ax.set_xticks([]); ax.set_yticks([])
plt.tight_layout()
plt.savefig('figures/problem-4-scatter.eps')


def vecPrint(V):
	print "\t".join([ "%.2f" % x for x in V ])
def matPrint(M): 
	for row in M: vecPrint(row)

def PCA(M):
	l,v = np.linalg.eig(M)
	proportion = l/np.sum(l)
	print "EIGENVALUES:"; vecPrint(l)
	print "PROPORTIONS:"; vecPrint(proportion)
	print "CUMULATIVE: "; vecPrint(np.cumsum(proportion))
	print "EIGENVECTOR:"; matPrint(v)
	return l,v

# ============== PART A  -- PCA w/ covariance matrix
cov = np.cov(data.T)
print "COVARIANCE MATRIX:"; matPrint(cov)
PCA(cov)

# ============== PART B  -- PCA w/ correlation matrix
corr = np.corrcoef(data.T)
print "="*50
print "CORRELATION MATRIX:"; matPrint(corr)
l,v = PCA(corr)

# ================ PART C --- Factor Analsyis 

# produce scree plot
ls = l.copy(); ls.sort(); ls = ls[::-1]
plt.clf()
fig,ax = plt.subplots(1,1)
fig.set_size_inches(16,10)
plt.plot(np.cumsum(np.ones(ls.shape)),ls,'ks-',markersize=10,linewidth=2)
plt.xlabel('factor'); plt.ylabel('eigenvalue')
plt.xlim(0,6)
l_sorted = l.sort()
plt.savefig('figures/problem-4-scree.eps')

# produce matrix of loading factors ...
m,n = 3 , len(l)
beta = np.sqrt(np.repeat(l[:m],n).reshape(m,n).T) * v[:,:m]
print "BETA"; matPrint(beta)
