"""
	TIME SERIES
	Munier Salem, Oct 1 2014

	Methods used in PS 3 to perform detrending,
	find the ACF / PCF etc ...
"""

import numpy as np


def get_acf(x,hmax=30):
	"""
		Computes autocovariance function
		by Definiton (1.5.2) of Brockwell & Davis 91
	"""
	xbar = np.average(x)
	acf = [np.average((x-xbar)**2)]
	for h in range(1,hmax):
		tmp = np.sum((x[h:]-xbar)*(x[:-h]-xbar))
		acf.append(tmp/(1.0*len(x)))
	return acf	


def mvg_avg(x,d):
	"""
	Applies moving average filter, following
	Eq. (1.4.16) of Brockwell & Davis 91 to 
	estimate the trend, given a best-guess 
	for the period of the seasion component (d)
	"""
	if d < 2: return x
	
	if d%2 == 0: 
		mt = .5*(x[:-d] + x[d:])
		for i in range(1,d): 
			mt += x[i:-(d-i)]
	else:
		raise TypeError('Odd terms not implemented') # FIXME  
	
	return mt/d


def get_seasonal(data,d):
	"""
	Given data and detrended data, computes
	the period-d seasonal component by 
	averaging every d'th data point from the 
	residuals between the data and the detrended
	"""
	
	# find smoothed trend via moving avg and 
	# detrend data 
	smooth = mvg_avg(data,d)
	detrend = data[d/2:-d/2] - smooth

	# using this, compute averaged seasonal
	# component 
	w = []
	for i in range(d):
		w.append(np.average(detrend[i::d]))
	w = np.array(w) - np.average(w)
	s = np.zeros(w.shape)
	for i,ww in enumerate(w):
		s[ (i + d/2)%d ] = ww

	# tile this result over whole length of 
	# raw data
	N = len(data)
	seasonal = np.concatenate(( np.tile(s,N/d), )) #, s[:N/d]))

	return smooth,detrend,seasonal


def detrend(data,d):
	"""
	Removes seasonal (d-period) and trend
	components from time series
	"""
	smooth,detrend,seasonal = get_seasonal(data,d)
	dssd = data - seasonal

	buffered = np.concatenate(([dssd[0]]*(d/2),dssd,[dssd[-1]]*(d/2)))
	trend = mvg_avg(buffered,d)

	return dssd - trend
