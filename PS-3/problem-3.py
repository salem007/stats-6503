"""
PROBLEM 3 / PSET 3
Munier Salem, Oct 12, 2014

Analyis of Strikes

- Computes ACF / PCF
- Fits AR(2) model

"""

import os
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm

def plot_acf_pcf(x,fig_name,n_lags=10):
	plt.clf()
	fig,((ax1),(ax2)) = plt.subplots(2,1)
	sm.graphics.tsa.plot_acf(x,lags=n_lags,ax=ax1)
	sm.graphics.tsa.plot_pacf(x,lags=n_lags,ax=ax2)
	plt.savefig(fig_name)

figMainDir,figDir,datDir = 'figures','figures/problem-3','data'
for dir in [figMainDir,figDir,datDir]:
  if not os.path.exists(dir): os.makedirs(dir)
lkwargs = {'loc' : 'best' , 'prop'  : {'size':6} }

data = np.loadtxt(datDir+'/strikes.txt')

# compute diff at lag one, twice
data = data[1:]-data[:-1]
data = data[1:]-data[:-1]

# plot the ACF/PCF
plot_acf_pcf(data,figDir+'/acf-pcf.eps')

# Fit an AR(2) model
arma_mod20 = sm.tsa.ARMA(data,(2,0)).fit()
labels,params = ['const','AR1','AR2'],arma_mod20.params
print "AR(2) MODEL: \n" + "-"*20
for l,p in zip(labels,params): print "%5s\t%.3f" % ( l,p )

# Plot ACF/PCF
plot_acf_pcf(arma_mod20.resid,figDir+'/AR2-acf-pcf.eps')

# Make a forecast / plot series
prediction = arma_mod20.predict(0,len(data)-1)
plot_acf_pcf(prediction,figDir+'/AR2-prediction-acf-pcf.eps')
plt.clf()
fig,((ax1),(ax2)) = plt.subplots(2,1)
ax1.plot(data)
ax1.plot(prediction)
ax1.legend(['Strikes (twice differenced)','AR(2) Model'],**lkwargs)
ax2.plot(arma_mod20.resid)
ax2.legend(['AR(2) residuals'],**lkwargs)
for ax in [ax1,ax2]: ax.set_xlim(0,len(prediction))
plt.savefig(figDir+'/strikes.eps')

# finally, display ARMA ACF
AR2_ACF = sm.tsa.stattools.acf(prediction)
print AR2_ACF
