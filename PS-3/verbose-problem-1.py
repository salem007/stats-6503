"""
	VERBOSE PROBLEM 1 / 2 
	Munier Salem
	Oct. 1 2014

		THIS VERSION OF PROBLEM 1 and 2:
			Performs detrending of data,
			plotting results of each step
			into saved figure 'figures/problem-X/detrended.eps'
"""
import os
import numpy as np
import matplotlib.pyplot as plt

#figDir = 'figures/problem-1A/'
#dataFile = 'data/JJ.txt'
#d = 4

figDir = 'figures/problem-1B/'
dataFile = 'data/deaths.txt'
d = 12 # seasonal period

def mvg_avg(x,d):
	"""
	Applies moving average filter, following
	Eq. (1.4.16) of Brockwell & Davis 91 to 
	estimate the trend, given a best-guess 
	for the period of the seasion component (d)
	"""
	if d < 2: return x
	
	if d%2 == 0: 
		mt = .5*(x[:-d] + x[d:])
		for i in range(1,d): 
			mt += x[i:-(d-i)]
	else:
		raise TypeError('Odd terms not implemented') # FIXME  
	
	return mt/d


def get_seasonal(data,d):
	"""
	Given data and detrended data, computes
	the period-d seasonal component by 
	averaging every d'th data point from the 
	residuals between the data and the detrended
	"""
	
	# find smoothed trend via moving avg and 
	# detrend data 
	smooth = mvg_avg(data,d)
	detrend = data[d/2:-d/2] - smooth

	# using this, compute averaged seasonal
	# component 
	w = []
	for i in range(d):
		w.append(np.average(detrend[i::d]))
	w = np.array(w) - np.average(w)
	s = np.zeros(w.shape)
	for i,ww in enumerate(w):
		s[ (i + d/2)%d ] = ww

	# tile this result over whole length of 
	# raw data
	N = len(data)
	seasonal = np.concatenate(( np.tile(s,N/d), )) #, s[:N/d]))

	return smooth,detrend,seasonal


# ================================
# ====== SCRIPT STARTS HERE ======
# ================================

if not os.path.exists(figDir): os.makedirs(figDir)
fig,((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)
lkwargs = {'loc' : 'best' , 'prop'  : {'size':6} }

# load data, take log return, plot
qtrly = np.log(np.loadtxt(dataFile))
ax1.plot(qtrly,'k-')

# remove seasonal component
smooth,detrend,seasonal = get_seasonal(qtrly,d)
deseasoned = qtrly - seasonal

ax1.plot(np.cumsum(np.ones(smooth.shape))+d/2-1,smooth,'k-',linewidth=2)
ax1.legend(['Raw Data','Moving Avg, d = %d'%d],**lkwargs)

ax2.plot(detrend,'k-')
ax2.plot(seasonal[d/2:-d/2],'k-',linewidth=2)
ax2.legend(['MA Residuals','Est. Seasonal Comp'],**lkwargs)

ax3.plot(deseasoned,'k-')

# Finally, estimate and remove trend
buffered = np.concatenate(([deseasoned[0]]*(d/2),deseasoned,[deseasoned[-1]]*(d/2)))
trend = mvg_avg(buffered,d)
detrend = deseasoned - trend
ax3.plot(trend,'k-',linewidth=2)
ax3.legend(['Deseasoned','Dss\'d, mvg avg d=%d'%d],**lkwargs)

ax4.plot(detrend,'k-')
ax4.legend(['Detrended'],**lkwargs)

plt.savefig(figDir+'detrended.eps')
