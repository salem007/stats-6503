"""
PROBLEM 1B / PSET 3
Munier Salem, Oct 1, 2014

Analyis of Monthly Accidental Deaths

- Detrends/deseasons 
- Computes ACF / PCF
- Fits ARIMA(3,3) model, forecasts
- Ljung-Box test applied to residuals

"""

import os
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import timeseries as ts

figMainDir,figDir,datDir = 'figures','figures/problem-1B','data'
for dir in [figMainDir,figDir,datDir]:
	if not os.path.exists(dir): os.makedirs(dir)
lkwargs = {'loc' : 'best' , 'prop'  : {'size':6} }

# load data, take log return, detrend 
raw = np.loadtxt(datDir+'/deaths.txt')
detrended = ts.detrend(raw,12) 

# subtract average
data = detrended - np.average(detrended)

# plot the ACF/PCF
plt.clf()
fig,((ax1),(ax2)) = plt.subplots(2,1)
sm.graphics.tsa.plot_acf(data,lags=30,ax=ax1)
sm.graphics.tsa.plot_pacf(data,lags=30,ax=ax2)
plt.savefig(figDir+'/acf-pcf.eps')

# Compute ARMA model
arma_mod21 = sm.tsa.ARMA(data,(2,1)).fit()

# Plot ACF/PCF
plt.clf()
fig,((ax1),(ax2)) = plt.subplots(2,1)
sm.graphics.tsa.plot_acf(arma_mod21.resid,lags=30,ax=ax1)
sm.graphics.tsa.plot_pacf(arma_mod21.resid,lags=30,ax=ax2)
plt.savefig(figDir+'/ARMA21-acf-pcf.eps')

# Make a forecast / plot ...
prediction = arma_mod21.predict(0,len(data)+36)
plt.clf()
fig,((ax1),(ax2)) = plt.subplots(2,1)
ax1.plot(data)
ax1.plot(prediction)
ax1.legend(['Detrended/Deseasoned Johnson & Johnson Qtrly Earnings',
	'ARMA(3,3) prediction'],**lkwargs)
ax2.plot(arma_mod21.resid)
ax2.legend(['ARMA(2,1) residuals'],**lkwargs)
for ax in [ax1,ax2]: ax.set_xlim(0,len(prediction))
plt.savefig(figDir+'/ARMA21-forecast.eps')

# Perform Ljung-Box test ...
(lbvalue,pvalue) = sm.stats.diagnostic.acorr_ljungbox(data,lags=10)
print lbvalue
print pvalue
