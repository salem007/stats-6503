"""
PROBLEM 2 / PSET 3
Munier Salem, Oct 12, 2014

Analyis of GNP Growth

- Computes ACF / PCF
- Fits AR(3) model

"""

import os
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm

NLAGS = 30

figMainDir,figDir,datDir = 'figures','figures/problem-2','data'
for dir in [figMainDir,figDir,datDir]:
  if not os.path.exists(dir): os.makedirs(dir)
lkwargs = {'loc' : 'best' , 'prop'  : {'size':6} }

data = np.loadtxt(datDir+'/gnp.txt')

# compute days for ea. data pt
dates = []
for date in data:
	dates.append("%4d-%02d-%02d" % tuple(date[:-1]))
dates = np.array(dates,dtype='datetime64[D]')
days   = (dates - dates[0])[:-1]

# compute growth rate
growth = np.log(data[:,-1])
growth = growth[1:] - growth[:-1]

# plot the ACF/PCF
plt.clf()
fig,((ax1),(ax2)) = plt.subplots(2,1)
sm.graphics.tsa.plot_acf(growth,lags=NLAGS,ax=ax1)
sm.graphics.tsa.plot_pacf(growth,lags=NLAGS,ax=ax2)
plt.savefig(figDir+'/acf-pcf.eps')

# Fit an AR(3) model
arma_mod30 = sm.tsa.ARMA(growth,(3,0)).fit()
labels,params = ['const','AR1','AR2','AR3'],arma_mod30.params
print "AR(3) MODEL: \n" + "-"*20
for l,p in zip(labels,params): print "%5s\t%.3f" % ( l,p )

# Plot ACF/PCF
plt.clf()
fig,((ax1),(ax2)) = plt.subplots(2,1)
sm.graphics.tsa.plot_acf(arma_mod30.resid,lags=NLAGS,ax=ax1)
sm.graphics.tsa.plot_pacf(arma_mod30.resid,lags=NLAGS,ax=ax2)
plt.savefig(figDir+'/AR3-acf-pcf.eps')

# Make a forecast / plot series
prediction = arma_mod30.predict(0,len(growth)-1)
plt.clf()
fig,((ax1),(ax2)) = plt.subplots(2,1)
ax1.plot(growth)
ax1.plot(prediction)
ax1.legend(['Growth Rate Data','AR(3) Model'],**lkwargs)
ax2.plot(arma_mod30.resid)
ax2.legend(['ARMA(3,3) residuals'],**lkwargs)
for ax in [ax1,ax2]: ax.set_xlim(0,len(prediction))
plt.savefig(figDir+'/growth-rate.eps')
