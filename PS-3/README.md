PROBLEM SET 3
=============

Munier Salem
------------

To generate figures and numbers in the writeup, run:

- verbose-problem-1.py
- problem-1A.py
- problem-1B.py
- problem-2.py

the first two rely on the following program I wrote:

- timeseries.py

and ALL THESE FILES require the following to be installed:

- numpy
- scipy
- pandas: http://pandas.pydata.org/
- statmodels: http://statsmodels.sourceforge.net/

The latter two can be installed on Mac OS X assuming the
latest command line tools are up to date with these commands:

			sudo easy_install pandas
			easy_install -U statsmodels

the latter relies on the former, and both rely on numpy/scipy 
